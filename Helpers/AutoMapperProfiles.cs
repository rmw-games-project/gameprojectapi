using System;
using AutoMapper;
using GamesProjectApi.Dtos;
using GamesProjectApi.Models;

namespace GamesProjectApi.Helpers {
	public class AutoMapperProfiles : Profile {
		public AutoMapperProfiles() {
			CreateMap<UserForUpdateDto, User>();
			CreateMap<User, UserDto>().ForMember(
				dest => dest.PhotoUrl,
				opt => {
					opt.MapFrom(src => ConvertPhoto(src));
				});
		}

		private static string ConvertPhoto(User user) {
			return Convert.ToBase64String(user.Photo);
		}
	}
}