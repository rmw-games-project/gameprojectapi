using System.Collections.Generic;
using System.Text.Json.Serialization;

public enum Role {
	Admin,
	Player
}

namespace GamesProjectApi.Models {
	
	public class User {
		public int Id { get; set; }
		public Role Role { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public byte[] Photo { get; set; }
		public string Status { get; set; }
		public string Color { get; set; }

		public byte[] PasswordHash { get; set; }
		public byte[] PasswordSalt { get; set; }
	}
}