using System;

namespace GamesProjectApi.Models {
	public class Message {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Text { get; set; }
		public DateTime Timestamp { get; set; }
		
		public int UserId { get; set; }
		
		public int SessionId { get; set; }
	}
}