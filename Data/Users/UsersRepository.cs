using System.Threading.Tasks;
using GamesProjectApi.Models;
using Microsoft.EntityFrameworkCore;

namespace GamesProjectApi.Data.Users {
	public class UsersRepository : IUsersRepository {
		private readonly DataContext _context;

		public UsersRepository(DataContext context) {
			_context = context;
		}
		
		public async Task<bool> SaveAll() {
			return await _context.SaveChangesAsync() > 0;
		}

		public async Task<User> GetUser(int id) {
			var user = await _context.Users.FirstOrDefaultAsync(
				x => x.Id == id);
			
			return user;
		}
		
		public async Task<User> GetUserByEmail(string email) {
			return await _context.Users
				.FirstOrDefaultAsync(x => x.Email == email);
		}

		public async Task<User[]> GetUsers() {
			var users = await _context.Users.ToArrayAsync();
			return users;
		}

		public async Task<User> AddUser(User user) {
			var createdUser = await _context.Users.AddAsync(user);
			await _context.SaveChangesAsync();
			return createdUser.Entity;
		}
		
		public async Task<bool> IsExists(string email) {
			if (await _context.Users.AnyAsync(x => x.Email == email))
				return true;

			return false;
		}
	}
}