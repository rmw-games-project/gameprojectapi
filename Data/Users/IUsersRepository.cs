using System.Threading.Tasks;
using GamesProjectApi.Models;

namespace GamesProjectApi.Data.Users {
	public interface IUsersRepository {
		Task<User> GetUser(int id);
		Task<User> GetUserByEmail(string email);
		Task<User[]> GetUsers();
		Task<User> AddUser(User user);
		Task<bool> IsExists(string email);
		Task<bool> SaveAll();
	}
}