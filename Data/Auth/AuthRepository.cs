using System.Threading.Tasks;
using GamesProjectApi.Data.Users;
using GamesProjectApi.Models;

namespace GamesProjectApi.Data.Auth
{
	public class AuthRepository : IAuthRepository {

		public AuthRepository(IUsersRepository usersRepo) {
			_usersRepo = usersRepo;
		}
		
		private readonly IUsersRepository _usersRepo;
		
		public async Task<User> Register(string email, string password)
		{
			byte[] passwordHash, passwordSalt;
			CreatePasswordHash(password, out passwordHash, out passwordSalt);
			
			var user = new User() {
				Role = Role.Player,
				Email = email,
				Name = "Anonymous",
				PasswordHash = passwordHash,
				PasswordSalt = passwordSalt
			};
			var createdUser = await _usersRepo.AddUser(user);
			return createdUser;
		}

		public async Task<User> Login(string email, string password)
		{
			var user = await _usersRepo.GetUserByEmail(email);
			if (user == null)
				return null;

			var isPasswordCorrect = VerityPasswordHash(
				password,
				user.PasswordHash,
				user.PasswordSalt
			);

			if (!isPasswordCorrect)
				return null;

			return await _usersRepo.GetUserByEmail(email);
		}

		public async Task<bool> UserExists(string email) {
			if (await _usersRepo.IsExists(email))
				return true;

			return false;
		}
		
		private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
		{
			using var hmac = new System.Security.Cryptography.HMACSHA512();
			passwordSalt = hmac.Key;
			passwordHash = hmac.ComputeHash(
				System.Text.Encoding.UTF8.GetBytes(password)
			);
		}
		
		private bool VerityPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
		{
			using var hmac = new System
				.Security
				.Cryptography
				.HMACSHA512(passwordSalt);

			var computedHash = hmac.ComputeHash(
				System.Text.Encoding.UTF8.GetBytes(password)
			);

			for (int i = 0; i < computedHash.Length; i++)
			{
				if (computedHash[i] != passwordHash[i]) return false;
			}

			return true;
		}
	}
}