using System.Threading.Tasks;
using GamesProjectApi.Models;

namespace GamesProjectApi.Data.Auth {
	public interface IAuthRepository {
		Task<User> Register(string email, string password);
		Task<User> Login(string email, string password);
		Task<bool> UserExists(string email);
	}
}