using System.ComponentModel.DataAnnotations;

namespace GamesProjectApi.Dtos {
	public class AuthDataDto {
		[Required]
		[EmailAddress]
		[MaxLength(50)]
		public string Email { get; set; }
		
		[Required]
		[MinLength(6)]
		[MaxLength(20)]
		public string Password { get; set; }
	}
}
