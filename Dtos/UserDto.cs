namespace GamesProjectApi.Dtos {
	public class UserDto {
		public int Id { get; set; }
		public Role Role { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string Status { get; set; }
		public string Color { get; set; }
		public string PhotoUrl { get; set; }
	}
}