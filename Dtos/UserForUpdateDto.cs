using System.ComponentModel.DataAnnotations;

namespace GamesProjectApi.Dtos {
	public class UserForUpdateDto {
		[Required]
		public int Id { get; set; }
		
		[Required]
		[MaxLength(30)]
		public string Name { get; set; }
		
		[Required]
		[EmailAddress]
		[MaxLength(50)]
		public string Email { get; set; }
	}
}