using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using GamesProjectApi.Data.Users;
using GamesProjectApi.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace GamesProjectApi.Controllers {
	
	[Authorize]
	[ApiController]
	[Route("api/[controller]")]
	public class UsersController : ControllerBase {

		public UsersController(
			IUsersRepository repository,
			IMapper mapper
		) {
			_repository = repository;
			_mapper = mapper;
		}
		
		private readonly IUsersRepository _repository;
		private readonly IMapper _mapper;

		[HttpGet]
		public async Task<IActionResult> GetUsers() {
			var users = await _repository.GetUsers();

			var usersDto = new UserDto[users.Length];
			for (int i = 0; i < users.Length; i++) {
				usersDto[i] = _mapper.Map<UserDto>(users[i]);
			}

			return Ok(usersDto);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetUser(int id) {
			var user = await _repository.GetUser(id);
			var mappedUser = _mapper.Map<UserDto>(user);
			return Ok(mappedUser);
		}
		
		[HttpPut("{id}/photo")]
		public async Task<IActionResult> UploadImage(int id, [FromForm]IFormFile file) {
			if (file == null) {
				return BadRequest();
			}
			
			var hasPermission = CheckHasPermission(id);
			if (!hasPermission) {
				return StatusCode(403);
			}
			
			var reader = new BinaryReader(file.OpenReadStream());
			var photo = reader.ReadBytes((int)file.Length);
			var user = await _repository.GetUser(id);
			user.Photo = photo;
			await _repository.SaveAll();
			
			return Ok();
		}

		[HttpPut]
		public async Task<IActionResult> UpdateUser(UserForUpdateDto userForUpdateDto) {
			var hasPermission = CheckHasPermission(userForUpdateDto.Id);
			if (!hasPermission) {
				return StatusCode(403);
			}
			
			var user = await _repository.GetUser(userForUpdateDto.Id);
			user = _mapper.Map(userForUpdateDto, user);
			await _repository.SaveAll();
			
			var mappedUser = _mapper.Map<UserDto>(user);
			return Ok(mappedUser);
		}

		private bool CheckHasPermission(int userId) {
			var role = User.FindFirst(ClaimTypes.Role).Value;
			var id = User.FindFirst("id").Value;
			return role == Role.Admin.ToString() || userId.ToString() == id;
		}
	}
}