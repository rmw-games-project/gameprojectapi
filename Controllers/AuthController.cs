using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using GamesProjectApi.Data.Auth;
using GamesProjectApi.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace GamesProjectApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AuthController : ControllerBase {
		
		public AuthController(
			IAuthRepository authRepo,
			IMapper mapper,
			IConfiguration config
		) {
			_authRepo = authRepo;
			_mapper = mapper;
			_config = config;
		}
		
		private readonly IAuthRepository _authRepo;
		private readonly IMapper _mapper;
		private readonly IConfiguration _config;

		[HttpPost("register")]
		public async Task<IActionResult> Register(AuthDataDto authData)
		{
			authData.Email = authData.Email.ToLower();

			if (await _authRepo.UserExists(authData.Email))
				return BadRequest("User with this email already exists");

			await _authRepo.Register(authData.Email, authData.Password);
			return await Login(authData);
		}

		[HttpPost("login")]
		public async Task<IActionResult> Login(AuthDataDto authData)
		{
			var user = await _authRepo.Login(authData.Email.ToLower(), authData.Password);
			
			if (user == null)
				return Unauthorized();

			var claims = new[] {
				new Claim("id", user.Id.ToString()), 
				new Claim(ClaimTypes.Role, user.Role.ToString()), 
				new Claim(ClaimTypes.Email, user.Email), 
				new Claim(ClaimTypes.Name, user.Name), 
			};
			
			var key = new SymmetricSecurityKey(
				Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value)
			);

			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(claims),
				Expires = DateTime.Now.AddDays(1),
				SigningCredentials = creds
			};

			var tokenHandler = new JwtSecurityTokenHandler();
			var token = tokenHandler.CreateToken(tokenDescriptor);
			var userDto = _mapper.Map<UserDto>(user);

			return Ok(new {
				token = tokenHandler.WriteToken(token),
				user = userDto
			});
		}
	}
}