using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GamesProjectApi.Data;
using GamesProjectApi.Data.Users;
using GamesProjectApi.Dtos;
using GamesProjectApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace GamesProjectApi.Hubs {

	[Authorize]
	public class ChatHub : Hub {
		private const int GeneralChatId = 0;
		private const int MessagesLoadAmount = 10;
		private const int MessagesIniAmount = 50;

		public ChatHub(
			IUsersRepository repository,
			DataContext context
		) {
			_repository = repository;
			_context = context;
		}
		
		private readonly IUsersRepository _repository;
		private readonly DataContext _context;

		public override async Task OnConnectedAsync() {
			Console.WriteLine("User connected: " + GetUserProp(ClaimTypes.Name));
			
			await Groups.AddToGroupAsync(Context.ConnectionId, "0");

			var messages = _context.Messages
				.Where(x => x.SessionId == GeneralChatId)
				.OrderByDescending(x => x.Timestamp)
				.Take(MessagesIniAmount)
				.OrderBy(x => x.Timestamp)
				.ToList();

			await Clients.Caller.SendAsync("onConnected", messages);
			await base.OnConnectedAsync();
		}

		public async Task Send(int sessionId, string messageText) {
			Console.WriteLine("sessionId: " + sessionId);
			
			// TODO checking is not forbidden to add message to this sessionId
			var message = new Message {
				UserId = int.Parse(GetUserProp("id")),
				SessionId = sessionId,
				Name = GetUserProp(ClaimTypes.Name),
				Text = messageText,
				Timestamp = DateTime.Now,
			};
			
			await _context.Messages.AddAsync(message);
			await _context.SaveChangesAsync();
			
			await Clients.Group(sessionId.ToString())
				.SendAsync("onMessage", message);
			Console.WriteLine(messageText);
		}

		public async Task LoadMoreMessages(int fromMessageId, int sessionId) {
			var messages = _context.Messages
				.Where(x => x.SessionId == sessionId)
				.Where(x => x.Id < fromMessageId)
				.OrderByDescending(x => x.Timestamp)
				.Take(MessagesLoadAmount)
				.OrderBy(x => x.Timestamp)
				.ToList();
			
			await Clients.Group(sessionId.ToString())
				.SendAsync("onMoreMessages", messages);
		}

		public override async Task OnDisconnectedAsync(Exception exception) {
			Console.WriteLine("User disconnected: " + GetUserProp(ClaimTypes.Name));
			
			
			
			await Clients.Caller.SendAsync("onDisconnected", 33);
			await base.OnDisconnectedAsync(exception);
		}

		private string GetUserProp(string claim) {
			return Context.User.FindFirst(claim).Value;
		}
	}
}